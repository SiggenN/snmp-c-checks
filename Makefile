CC=gcc
CFLAGS=`net-snmp-config --cflags`
LDFLAGS=`net-snmp-config --libs`
SOURCES=$(wildcard *.c)
BINS=$(SOURCES:.c=)
INSDIR=/opt/netnordic/plugins
ifdef DESTDIR
	INSDIR=${DESTDIR}/opt/netnordic/plugins
endif

all: $(BINS)

$(BINS): %: %.c
	$(CC) $< ${CFLAGS} ${LDFLAGS} -o $@

install: $(BINS)
	install -d -m 0755 $(INSDIR)
	install -m 0755 $^ $(INSDIR)
	install -m 0755 *.sh $(INSDIR)

clean:
	rm -f $(BINS)
