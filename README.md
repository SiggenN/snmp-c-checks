# snmp-c-checks

compiled checks using snmp
requires net-snmp library to compile, easiest is to install libsnmp-dev (debian flavours) or net-snmp-devel (red hats and lookalikes)
`make all` will compile all .c files in current dir to separate binaries
`make install` installs them under /usr/lib/netnordic
