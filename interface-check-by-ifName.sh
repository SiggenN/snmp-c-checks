#!/bin/bash

OID_IFNAME='1.3.6.1.2.1.31.1.1.1.1'
BIN="/opt/netnordic/plugins/interface-check"
usage() { echo """
Example:
./interface-check-by-ifName -i ifname -H ip -M mode -C community
"""
exit 0
}

while getopts ":H:M:C:i:" arg; do
    case ${arg} in
        H )
            HOST=${OPTARG}
            ;;
        M )
            MODE=${OPTARG}
            ;;
        C )
            COMMUNITY=${OPTARG}
            ;;
        i )
            IFNAME=${OPTARG}
            ;;

        * )
            usage
            ;;
    esac
done

#some small sanity check of arguments
if [[ -z "${COMMUNITY}" || -z "${HOST}"
|| -z "${MODE}" || -z "${IFNAME}" ]]; then
    usage
fi
#options to pass to interface-check
cmdopts="-H ${HOST} -M ${MODE} -C ${COMMUNITY}"

snmpWalkData=$(snmpwalk -v 2c ${HOST} -O qn -c ${COMMUNITY} $OID_IFNAME)



while read -r line; do
    #check if name matches
    name=$(echo "$line" | cut -d " " -f 2 | tr -d '"')
    if [ "$name" == "${IFNAME}" ]; then
        oid=$(echo "$line" | cut -d " " -f 1)
        #get index after last "."
        if_index=${oid##*.}
        $BIN $cmdopts -i $if_index
        exit $? #exit with exit code from interface-check bin
    fi
done <<< "$snmpWalkData"
#if we got here looks like we didn't find any interface, exit with unknown
echo "no entry found in IfName table that equals ${IFNAME}"
exit 3
