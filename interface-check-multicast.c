#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <float.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#define LOGDIR "/tmp/multicast"

#define MULTICAST_IN_OID "1.3.6.1.2.1.31.1.1.1.2"

#define PACKET_SIZE 188 //188 byte in för FEC, annars 204 bytes

#define TRAFFIC_MODE 1
#define STATUS_MODE 2
#define ERROR_MODE 3

#define USAGESTR "Usage: [-MHCdiwcb] \n M = Mode (1 multicast)\n H = ip-address\n i = interfaceId\n C = SNMP Community string \n "\
"d = debug mode\n\nSNMP-MULTICAST-OPTIONS:\n w = warning speed in %%\n c = critical speed in %%\n b = max interface speed mbits\n\n"

struct settings {
    char host[100];
    int mode;
    bool debugMode;
    char communityString[100];
    int interfaceId;
    float warningLimit;
    float criticalLimit;
    int interfaceSpeed;
    uint64_t inCounter;
    uint64_t outCounter;
    uint64_t inDifference;
    uint64_t outDifference;

    uint64_t inErrorCounter;
    uint64_t inDiscardCounter;
    uint64_t outErrorCounter;
    uint64_t outDiscardCounter;
    uint64_t inErrorDifference;
    uint64_t inDiscardDifference;
    uint64_t outErrorDifference;
    uint64_t outDiscardDifference;

    uint64_t timeDifference;
    int ifStatus;
    int firstRun;
};
typedef struct settings Settings;

void parseArguments(Settings *settings, int argc, char *argv[]);
uint64_t makeSNMPGet(Settings *settings, char SNMPoid[]);
int writeResultToFile(Settings *settings);
int calculateExitValue(Settings *settings);

int main(int argc, char *argv[])
{
    //Default settings
    Settings settings = { "10.16.64.3", 1, false, "public", 1, 80, 90, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    settings.host[sizeof(settings.host)] = '\0'; //Null terminate since strncpy do not nullterminate if the string is to long
    settings.host[sizeof(settings.communityString)] = '\0';
    parseArguments(&settings, argc, argv);

    if(settings.debugMode){
        printf("--------Entered debugmode--------\n");
        printf("Host: %s\n", settings.host);
        printf("Community string: %s\n", settings.communityString);
        printf("Interface id: %d\n", settings.interfaceId);
    }

    settings.inCounter = makeSNMPGet(&settings, MULTICAST_IN_OID);

    if(settings.mode == TRAFFIC_MODE || settings.mode == ERROR_MODE){
        writeResultToFile(&settings);
    }

    return calculateExitValue(&settings);
}

void parseArguments(Settings *settings, int argc, char *argv[]){
    if(argc < 2){
        fprintf(stderr, USAGESTR);
        exit(EXIT_FAILURE);
    }

    int opt;
    while ((opt = getopt(argc, argv, "H:M:dC:i:w:c:b:")) != -1) {
        switch (opt) {
        case 'H': strncpy(settings->host, optarg, sizeof(settings->host) - 1); break;
        case 'M': settings->mode = atoi(optarg); break;
        case 'd': settings->debugMode = true; break;
        case 'C': strncpy(settings->communityString, optarg, sizeof(settings->communityString) - 1); break;
        case 'i': settings->interfaceId = atoi(optarg); break;
        case 'w': settings->warningLimit = atoi(optarg); break;
        case 'c': settings->criticalLimit = atoi(optarg); break;
        case 'b': settings->interfaceSpeed = atoi(optarg); break;
        default:
            fprintf(stderr, USAGESTR);
            exit(EXIT_FAILURE);
        }
    }

    //Validation
    if(settings->interfaceId < 1){
        fprintf(stderr, "InterfaceId can't be less than 1\n");
        exit(EXIT_FAILURE);
    }

}

uint64_t makeSNMPGet(Settings *settings, char SNMPoid[]){
    //src, but modified:http://www.net-snmp.org/wiki/index.php/TUT:Simple_Application
    struct snmp_session session, *ss;
    struct snmp_pdu *pdu;
    struct snmp_pdu *response;

    oid anOID[MAX_OID_LEN];
    size_t anOID_len = MAX_OID_LEN;

    struct variable_list *vars;
    int status;

    //Initialize the SNMP library
    init_snmp("snmpapp");

    snmp_sess_init( &session );
    //set up defaults
    session.peername = settings->host;

    /* set the SNMP version number */
    session.version = SNMP_VERSION_2c;
    /* set the SNMPv1 community name used for authentication */
    session.community = settings->communityString;
    session.community_len = strlen(session.community);

    SOCK_STARTUP;

    //Establish session
    ss = snmp_open(&session);

    if (!ss) {
        snmp_perror("ack");
        snmp_log(LOG_ERR, "Could not open SNMP session!\n");
        exit(3);
    }

    pdu = snmp_pdu_create(SNMP_MSG_GET);

    char oid[256];
    snprintf(oid, sizeof(oid), "%s.%d", SNMPoid, settings->interfaceId);
    read_objid(oid, anOID, &anOID_len);

    snmp_add_null_var(pdu, anOID, anOID_len);
    //Send request
    status = snmp_synch_response(ss, pdu, &response);

    int64_t counter;

    if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
        for(vars = response->variables; vars; vars = vars->next_variable)
            //print_variable(vars->name, vars->name_length, vars);
            usleep(1); // Wait 1ms so everything is ready
            /* manipulate the information ourselves */
            for(vars = response->variables; vars; vars = vars->next_variable) {
                int count=1;
                if (vars->type == ASN_COUNTER64) {
                    //TODO get ASN_COUNTER to work properly
                    //printf("value is a 64bit  counter\n");
                    counter = (uint64_t) vars->val.counter64->high << 32 | vars->val.counter64->low;
                }
                else if (vars->type == ASN_COUNTER){
                    //printf("Value is a 32bit counter\n");
                    counter = (uint64_t) *vars->val.integer;
                }
                else if (vars->type == ASN_INTEGER){
                    counter = (uint64_t) *vars->val.integer;
                }
                else{
                    printf("snmp value is of a unknown data type!\n");
                    exit(3);
                }

            }
    } else {
     /*
      * FAILURE: print what went wrong!
      */

        if (status == STAT_SUCCESS)
            fprintf(stderr, "Error in packet\nReason: %s\n",
               snmp_errstring(response->errstat));
        else
            snmp_sess_perror("snmpget", ss);
        
        exit(3);
   }

    /*
    * Clean up:
    *  1) free the response.
    *  2) close the session.
    */
    if (response)
        snmp_free_pdu(response);
    snmp_close(ss);

    /* windows32 specific cleanup (is a noop on unix) */
    SOCK_CLEANUP;
    return counter;
}

int writeResultToFile(Settings *settings){
    //Create dir
    struct stat st;
    if (stat(LOGDIR, &st)) {
        mkdir(LOGDIR, 0700);
        printf("Created dir\n");
    }

    if (settings->mode == TRAFFIC_MODE){
        //Create file
        char filepath[100];
        snprintf(filepath, sizeof(filepath), "%s/%s__%d", LOGDIR, settings->host, settings->interfaceId);

        if (stat(filepath, &st)) {
            FILE *create = fopen(filepath, "w");
            fclose(create);
            printf("Created file\n");
            settings->firstRun = 1;
        }

        FILE *rfp = fopen(filepath, "r+");
        if (rfp == NULL){
            perror("Failed to read result file");
            exit(3);
        }
        //File format is:
        //Epoch time
        //InTrafficData
        //OutTrafficData
        uint64_t oldInResult = 0, oldOutResult = 0, timeStamp = 0;
        if (!settings->firstRun){
            fscanf(rfp, "%lld", &timeStamp);
            fscanf(rfp, "%lld", &oldInResult);
            fscanf(rfp, "%lld", &oldOutResult);
            fclose(rfp);

            //Calculate difference from old value and the new value
            settings->timeDifference = time(NULL) - timeStamp;
            settings->inDifference = settings->inCounter - oldInResult;
            settings->outDifference = settings->outCounter - oldOutResult;
            //printf("This check ran %lld secounds ago  ", settings->timeDifference);
            if(settings->debugMode){
                printf("inDifferance is %lld\n", settings->inDifference);
                printf("outDifferance is %lld\n", settings->outDifference);
            }
        }

        //Write to file

        FILE *wfp = fopen(filepath, "w");
        if (wfp == NULL){
            perror("Failed to write to result file");
            exit(3);
        }
        fprintf(wfp, "%lld\n", time(NULL));
        fprintf(wfp, "%lld\n", settings->inCounter);
        fprintf(wfp, "%lld\n", settings->outCounter);

        fclose(wfp);

        if ((oldInResult > settings->inCounter) || (oldOutResult > settings->outCounter)){
            printf("Reseting result file");
            settings->firstRun = 1;
        }
        
    }
    if (settings->mode == ERROR_MODE){
        //Create file
        char filepath[100];
        snprintf(filepath, sizeof(filepath), "%s/%s__%d_error", LOGDIR, settings->host, settings->interfaceId);

        if (stat(filepath, &st)) {
            FILE *create = fopen(filepath, "w");
            fclose(create);
            printf("Created file\n");
            settings->firstRun = 1;
        }

        FILE *rfp = fopen(filepath, "r+");
        if (rfp == NULL){
            perror("Failed to read result file");
            exit(3);
        }
        //File format is:
        //Epoch time
        //inErrorCounter
        //inDiscardCounter
        //outErrorCounter
        //outDiscardCounter
        uint64_t oldInErrorCounter = 0, oldInDiscardCounter = 0, oldOutErrorCounter = 0, oldOutDiscardCounter = 0, timeStamp = 0;
        if (!settings->firstRun){
            fscanf(rfp, "%lld", &timeStamp);
            fscanf(rfp, "%lld", &oldInErrorCounter);
            fscanf(rfp, "%lld", &oldInDiscardCounter);
            fscanf(rfp, "%lld", &oldOutErrorCounter);
            fscanf(rfp, "%lld", &oldOutDiscardCounter);
            fclose(rfp);

            //Calculate difference from old value and the new value
            settings->timeDifference = time(NULL) - timeStamp;
            settings->inErrorDifference = settings->inErrorCounter - oldInErrorCounter;
            settings->inDiscardDifference = settings->inDiscardCounter - oldInDiscardCounter;
            settings->outErrorDifference = settings->outErrorCounter - oldOutErrorCounter;
            settings->outDiscardDifference = settings->outDiscardCounter - oldOutDiscardCounter;

            if(settings->debugMode){
                printf("This check ran %lld secounds ago  ", settings->timeDifference);
                printf("inErrorDifferance is %lld\n", settings->inErrorDifference);
                printf("outErrorDifferance is %lld\n", settings->outErrorDifference);
            }
        }

        //Write to file

        FILE *wfp = fopen(filepath, "w");
        if (wfp == NULL){
            perror("Failed to write to result file");
            exit(3);
        }
        fprintf(wfp, "%lld\n", time(NULL));
        fprintf(wfp, "%lld\n", settings->inErrorCounter);
        fprintf(wfp, "%lld\n", settings->inDiscardCounter);
        fprintf(wfp, "%lld\n", settings->outErrorCounter);
        fprintf(wfp, "%lld\n", settings->outDiscardCounter);

        fclose(wfp);

        if ((oldInErrorCounter > settings->inErrorCounter) || (oldInDiscardCounter > settings->inDiscardCounter) || (oldOutErrorCounter > settings->outErrorCounter) || (oldOutDiscardCounter > settings->outDiscardCounter)){
            printf("Reseting result file");
            settings->firstRun = 1;
            return 0;
        }
    }
}

uint64_t bytesToMb(uint64_t b){
    uint64_t result = (b * 8) / 1000000;
    return result;
}

int calculateExitValue(Settings *settings){
    int exitValue = 0;
    if (settings->firstRun){
        return 0;
    }


    if (settings->timeDifference < 1){
        //Avoid divide by 0
        printf("Wait before you run the check again");
        return 0;
    }

    uint64_t mbitResult = bytesToMb((settings->inDifference * PACKET_SIZE)) / settings->timeDifference;

    if (1 == 0){ //TODO: add real condition
        printf("Critical: ");
        exitValue =  2;
    }
    else{
        printf("Ok: ");
    }

    printf("in multicast traffic %lld Mbit/s", mbitResult);

    printf("|Interface-%d-in=%lld;%.2f;%.2f;;", settings->interfaceId, mbitResult, settings->warningLimit, settings->criticalLimit);

    return exitValue;
}
